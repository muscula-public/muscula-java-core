package com.muscula.core.model;

import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

class MusculaDTOTest {

    @Test
    public void structuralDataTest() {
        MusculaDTO dto = new MusculaDTO("123", new MusculaLog("msg", "error", "MyStringValue", new LogParam("longParam", 12345L)));
        JSONAssert.assertEquals("{\"structuralData\":{\"data\":[{\"name\":\"param_0\",\"value\":\"MyStringValue\"},{\"name\":\"longParam\",\"value\":12345}]}}", dto.asJson(), false);

        // no structural data
        dto = new MusculaDTO("123", new MusculaLog("msg", "error"));
        JSONAssert.assertNotEquals("{\"structuralData\":{}}", dto.asJson(), false);

        // no structural data
        dto = new MusculaDTO("123", new MusculaLog("msg", "error", null, null));
        JSONAssert.assertNotEquals("{\"structuralData\":{}}", dto.asJson(), false);

        // structural data (params + simple exception)
        dto = new MusculaDTO("123", new MusculaLog("msg", "error", new RuntimeException("Test exception"), new LogParam("longParam", 12345L)));
        JSONAssert.assertEquals("{\"structuralData\":{\"data\":[{\"name\":\"longParam\",\"value\":12345}]}}", dto.asJson(), false);

        // structural data (params + MusculaThrowableProxy without param)
        dto = new MusculaDTO("123", new MusculaLog("msg", "error", new MusculaThrowableProxy(new RuntimeException("Test exception")), new LogParam("longParam", 12345L)));
        JSONAssert.assertEquals("{\"structuralData\":{\"data\":[{\"name\":\"longParam\",\"value\":12345}]}}", dto.asJson(), false);

        // structural data (params + MusculaThrowableProxy with params)
        dto = new MusculaDTO("123", new MusculaLog("msg", "error", new MusculaThrowableProxy(new RuntimeException("Test exception"), 1234L, new LogParam("stringParam", "string Val")), new LogParam("longParam", 12345L)));
        JSONAssert.assertEquals("{\"structuralData\":{\"data\":[{\"name\":\"longParam\",\"value\":12345},{\"name\":\"param_1\",\"value\":1234},{\"name\":\"stringParam\",\"value\":\"string Val\"}]}}", dto.asJson(), false);

    }

}
