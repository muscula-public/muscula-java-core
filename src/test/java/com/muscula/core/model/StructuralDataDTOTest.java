package com.muscula.core.model;

import com.muscula.core.model.LogParam;
import com.muscula.core.model.StructuralDataDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

class StructuralDataDTOTest {

    @Test
    public void structuralDataAsJsonTest() {

        //simple object
        StructuralDataDTO sd = new StructuralDataDTO(new TestClass("test", 65535L));
        JSONAssert.assertEquals("{\"x\":\"test\",\"y\":65535}", sd.asJson().toString(), true);

        // the same object wrapped by LogParam
        sd = new StructuralDataDTO(new LogParam("myParam", new TestClass("test", 65535L)));
        JSONAssert.assertEquals("{\"data\":[{\"name\":\"myParam\",\"value\":{\"x\":\"test\",\"y\":65535}}]}", sd.asJson().toString(), true);

        // double simple objects, should autogenerate params names
        sd = new StructuralDataDTO(new TestClass("test", 65535L), new TestClass("test2", 256L));
        JSONAssert.assertEquals("{\"data\":[{\"name\":\"param_0\",\"value\":{\"x\":\"test\",\"y\":65535}},{\"name\":\"param_1\",\"value\":{\"x\":\"test2\",\"y\":256}}]}", sd.asJson().toString(), true);

        //object inside object
        sd = new StructuralDataDTO(new TestClass2(128, new TestClass("test", 65535L)));
        JSONAssert.assertEquals("{\"obj\":{\"x\":\"test\",\"y\":65535},\"z\":128}", sd.asJson().toString(), true);

        //object inside object + LogParam
        sd = new StructuralDataDTO(new TestClass2(128, new TestClass("test", 65535L)), new LogParam("myParam", "myValue"));
        JSONAssert.assertEquals("{\"data\":[{\"name\":\"param_0\",\"value\":{\"obj\":{\"x\":\"test\",\"y\":65535},\"z\":128}},{\"name\":\"myParam\",\"value\":\"myValue\"}]}", sd.asJson().toString(), true);

        //null value
        sd = new StructuralDataDTO(null);
        Assertions.assertFalse(sd.hasData());
        Assertions.assertNull(sd.asJson());

        // Log param with null value
        sd = new StructuralDataDTO(new LogParam("myParam", null));
        JSONAssert.assertEquals("{\"data\":[{\"name\":\"myParam\"}]}", sd.asJson().toString(), true);

    }

    public class TestClass {
        private String x;
        private Long y;

        public TestClass(String x, Long y) {
            this.x = x;
            this.y = y;
        }

        public String getX() {
            return x;
        }

        public Long getY() {
            return y;
        }
    }

    public class TestClass2 {
        private int z;
        private TestClass obj;

        public TestClass2(int z, TestClass obj) {
            this.z = z;
            this.obj = obj;
        }

        public int getZ() {
            return z;
        }

        public TestClass getObj() {
            return obj;
        }
    }
}
