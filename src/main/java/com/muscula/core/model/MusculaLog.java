package com.muscula.core.model;

public class MusculaLog {

    private String message;
    private Throwable exception;
    private String severity;
    private Object[] params;

    public MusculaLog(String message, String severity, Throwable exception) {
        this(message, severity);
        this.exception = exception;
    }

    public MusculaLog(String message, String severity) {
        this.message = message;
        this.severity = severity;
    }

    public MusculaLog(String message, String severity, Throwable exception, Object ... params) {
        this.message = message;
        this.exception = exception;
        this.severity = severity;
        this.params = params;
    }

    public MusculaLog(String message, String severity, Object ... params) {
        this.message = message;
        this.severity = severity;
        this.params = params;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getException() {
        return exception;
    }

    public String getSeverity() {
        return severity;
    }

    public Object[] getParams() {
        return params;
    }
}
