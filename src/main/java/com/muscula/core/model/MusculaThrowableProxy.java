package com.muscula.core.model;

public class MusculaThrowableProxy extends Throwable{

    private Object data;
    private LogParam[] params;

    public MusculaThrowableProxy(String s, Throwable throwable, Object object) {
        super(s, throwable);
        this.data = object;
    }

    public MusculaThrowableProxy(Throwable throwable, Object object) {
        super(throwable);
        this.data = object;
    }

    public MusculaThrowableProxy(String s, Throwable throwable, Object data, LogParam ... params) {
        super(s, throwable);
        this.data = data;
        this.params = params;
    }

    public MusculaThrowableProxy(Throwable throwable, Object data, LogParam ... params) {
        super(throwable);
        this.data = data;
        this.params = params;
    }

    public MusculaThrowableProxy(String s, Throwable throwable, LogParam ... params) {
        super(s, throwable);
        this.params = params;
    }

    public MusculaThrowableProxy(Throwable throwable, LogParam ... params) {
        super(throwable);
        this.params = params;
    }

    public Object getData() {
        return data;
    }

    public LogParam[] getParams() {
        return params;
    }
}
