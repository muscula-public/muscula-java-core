package com.muscula.core.model;

import com.muscula.core.Severity;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MusculaDTO {

    private final String logId;
    private final String severity;
    private Integer lineNumber;
    private String fileName;
    private String message;
    private String exception;
    private List<String> stackTrace = Collections.emptyList();
    private final StructuralDataDTO structuralData;

    public MusculaDTO(String logId, MusculaLog musculaLog) {
        this.logId = logId;
        this.message = musculaLog.getMessage();
        this.severity = musculaLog.getSeverity();
        this.structuralData = new StructuralDataDTO(musculaLog.getParams());
        if(musculaLog.getException() instanceof MusculaThrowableProxy) {
            retrieveDataFromException((MusculaThrowableProxy) musculaLog.getException());
        } else {
            retrieveDataFromException(musculaLog.getException());
        }
    }


    private void retrieveDataFromException(MusculaThrowableProxy exception) {
        if (exception != null) {
            retrieveDataFromException(exception.getCause());
            retrieveStructuralData(exception);

        }
    }

    private void retrieveDataFromException(Throwable exception) {
        if (exception != null) {
            this.exception = getExceptionAsString(exception);
            retrieveDataFromStackTrace(exception.getStackTrace());
        }
    }

    private String getExceptionAsString(Throwable exception) {
        if (exception == null) return "";
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);
        return sw.toString().replaceAll("[\\t\\n]", " ");
    }

    private void retrieveDataFromStackTrace(StackTraceElement[] stackTrace) {
        if (stackTrace != null && stackTrace.length > 0) {
            this.lineNumber = stackTrace[0].getLineNumber();
            this.fileName = stackTrace[0].getFileName();
            this.stackTrace = Stream.of(stackTrace).map(StackTraceElement::toString).collect(Collectors.toList());
        }
    }

    private void retrieveStructuralData(MusculaThrowableProxy errorProxy) {
        structuralData.addParam(errorProxy.getData());
        structuralData.addParams(errorProxy.getParams());
    }

    public String asJson() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("logId", logId);
        jsonObject.put("severity", Severity.Map(severity));
        jsonObject.put("lineNumber", lineNumber);
        jsonObject.put("fileName", fileName);
        jsonObject.put("message", message);
        jsonObject.put("exception", exception);
        jsonObject.put("stackTrace", stackTrace);
        if (structuralData.hasData()) {
            jsonObject.put("structuralData", structuralData.asJson());
        }

        return jsonObject.toString();
    }
}
