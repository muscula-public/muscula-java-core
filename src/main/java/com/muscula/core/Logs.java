package com.muscula.core;

import com.muscula.core.model.MusculaLog;

public class Logs {

    private Logs() {}

    public static void push(MusculaLog musculaLog) {
        ErrorBuffer.INSTANCE.push(musculaLog);
    }

}
