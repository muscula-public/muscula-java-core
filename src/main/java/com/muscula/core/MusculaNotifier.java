package com.muscula.core;

import com.muscula.core.model.MusculaLog;
import com.muscula.core.model.MusculaDTO;
import okhttp3.*;

import java.io.IOException;
import java.util.logging.Logger;

class MusculaNotifier {
    private static Logger logger = Logger.getLogger(MusculaNotifier.class.getName());
    private static final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    public void notify(MusculaLog musculaLog) {
        if(Credentials.getLogId() == null || Credentials.getLogId().isEmpty()) {
            logger.info("Muscula log ID is required. Please complete configuration");
            return;
        }

        sendPost(musculaLog);
    }

    private void sendPost(MusculaLog musculaLog) {
        try {
            OkHttpClient client = new OkHttpClient();
            RequestBody body = RequestBody.create(new MusculaDTO(Credentials.getLogId(), musculaLog).asJson(), JSON);
            Request request = new Request.Builder()
                    .url(Credentials.getEndpointUrl())
                    .post(body)
                    .build();
            try (Response response = client.newCall(request).execute()) {
                if (!response.isSuccessful() || response.code() > 200) {
                    System.err.println("Unsuccessful response from muscula. [" + response.code() + "]. Message: " + response.message());
                }
            } catch (IOException ex) {
                System.err.println("Exception while sending request to muscula " + ex);
            }
        } catch (Exception e) {
            System.err.println("Exception while preparing request for muscula " + e);
        }
    }
}
