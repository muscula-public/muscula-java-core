package com.muscula.core;

public class Credentials {

    private static String logId;
    private static String endpointUrl;

    private Credentials() {}

    public static void setLogId(String logId) {
        Credentials.logId = logId;
    }

    public static void setEndpointUrl(String endpointUrl) {
        Credentials.endpointUrl = endpointUrl;
    }

    public static String getLogId() {
        return logId;
    }

    public static String getEndpointUrl() {
        if (endpointUrl == null || endpointUrl.isEmpty()) {
            return "https://harvester.muscula.com/log";
        }
        return endpointUrl;
    }
}
