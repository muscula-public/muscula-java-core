package com.muscula.core;

import com.muscula.core.model.MusculaLog;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 *
 * Singleton class represents errors buffer. Max buffer size is defined as 10000. <br/>
 * If increasing errors count is much faster then processing these errors, after the buffer is clogged - new errors are ignored.
 */
enum ErrorBuffer {

    INSTANCE;

    private static final int DEFAULT_MAX_QUEUE_SIZE = 10000;
    private final BlockingQueue<MusculaLog> queue;
    private final LogConsumer consumer;

    ErrorBuffer(int bufferSize) {
        queue = new LinkedBlockingQueue<>(bufferSize);
        this.consumer = new LogConsumer(queue);
    }

    ErrorBuffer() {
        this(DEFAULT_MAX_QUEUE_SIZE);
    }

    /**
     * Push new error to queue <br/>
     * @param musculaLog
     */
    public void push(MusculaLog musculaLog) {
        if(musculaLog == null) return;

        boolean added = queue.offer(musculaLog);
        if(added) {
            consumer.consume();
        }
    }
}
