package com.muscula.core;

import com.muscula.core.model.MusculaLog;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

final class LogConsumer {

    private final BlockingQueue<MusculaLog> queue;
    private final MusculaNotifier musculaNotifier;
    private final AtomicBoolean running;
    private final AtomicBoolean requested;

    public LogConsumer(BlockingQueue<MusculaLog> queue) {
        this.queue = queue;
        this.musculaNotifier = new MusculaNotifier();
        this.running = new AtomicBoolean();
        this.requested = new AtomicBoolean();
    }

    /**
     * Notify consumer about new error to consume <br/>
     * Errors in queue are processed by separated one thread which is shutdown after one second of inactivity in queue. <br/>
     * After one second of inactivity when new error occurs, new thread is created.
     */
    public void consume() {
        requested.set(true);

        if (running.compareAndSet(false, true)) {
            final ExecutorService executorService = Executors.newSingleThreadExecutor();
            executorService.execute(() -> {
                while (true) {
                    try {
                        requested.set(false);
                        MusculaLog musculaLog = queue.poll(1, TimeUnit.SECONDS);
                        if (musculaLog == null && !requested.get()) {
                            running.set(false);
                            executorService.shutdown();
                            return;
                        } else {
                            musculaNotifier.notify(musculaLog);
                        }

                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    } catch (Exception e) {
                        System.err.println(e);
                    }
                }
            });
        }
    }

}
