package com.muscula.core;

import java.util.HashMap;
import java.util.Map;

/*
    Muscula severity mapping
    Fatal = 0,
    Error = 1,
    Warning = 2,
    Info= 3 ,
    Debug = 4,
    Trace = 5,
 */
public class Severity {
    public static final Map<String, Byte> staticMap = new HashMap<>();

    static {
        staticMap.put("ALERT", (byte) 0);
        staticMap.put("CRITICAL", (byte) 0);
        staticMap.put("DEBUG", (byte) 4);
        staticMap.put("EMERG", (byte) 0);
        staticMap.put("ERROR", (byte) 1);
        staticMap.put("INFO", (byte) 3);
        staticMap.put("NOTICE", (byte) 3);
        staticMap.put("WARNING", (byte) 2);
        staticMap.put("WARN", (byte) 2);
        staticMap.put("TRACE", (byte) 5);
    }

    public static byte Map(String severity) {
        if (severity == null) return 1;
        return staticMap.getOrDefault(severity, (byte) 1);
    }
}
